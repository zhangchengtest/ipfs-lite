package threads.lite.cid;

import androidx.annotation.NonNull;
import androidx.room.TypeConverter;

import java.security.MessageDigest;
import java.util.Arrays;

import threads.lite.utils.DataHandler;


public final class ID implements Comparable<ID> {
    public final byte[] data;

    public ID(@NonNull byte[] data) {
        this.data = data;
    }

    @NonNull
    @TypeConverter
    public static ID fromArray(byte[] data) {
        if (data == null) {
            throw new IllegalStateException("data can not be null");
        }
        return new ID(data);
    }

    @NonNull
    @TypeConverter
    public static byte[] toArray(ID id) {
        if (id == null) {
            throw new IllegalStateException("id can not be null");
        }
        return id.data;
    }


    @NonNull
    public static ID convertPeerID(@NonNull PeerId id) throws Exception {
        return convertKey(id.getBytes());
    }

    @NonNull
    public static ID convertKey(@NonNull byte[] id) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        return new ID(digest.digest(id));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ID id = (ID) o;
        return Arrays.equals(data, id.data);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(data);
    }

    @Override
    public int compareTo(@NonNull ID o) {
        return DataHandler.compareUnsigned(this.data, o.data);
    }
}

package threads.lite.crypto;

import androidx.annotation.NonNull;

import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.bouncycastle.crypto.signers.Ed25519Signer;

import crypto.pb.Crypto;


public class Ed25519 {

    public static PubKey unmarshalEd25519PublicKey(byte[] keyBytes) {
        return new Ed25519PublicKey(new Ed25519PublicKeyParameters(keyBytes, 0));
    }

    public static final class Ed25519PublicKey extends PubKey {
        private final Ed25519PublicKeyParameters publicKeyParameters;

        public Ed25519PublicKey(Ed25519PublicKeyParameters publicKeyParameters) {
            super(Crypto.KeyType.Ed25519);
            this.publicKeyParameters = publicKeyParameters;
        }


        @NonNull
        public byte[] raw() {
            return this.publicKeyParameters.getEncoded();
        }

        public void verify(byte[] data, byte[] signature) throws Exception {

            Ed25519Signer ed25519Signer = new Ed25519Signer();
            ed25519Signer.init(false, this.publicKeyParameters);
            ed25519Signer.update(data, 0, data.length);
            boolean result = ed25519Signer.verifySignature(signature);
            if (!result) {
                throw new Exception("verify failed");
            }
        }

        public int hashCode() {
            return this.publicKeyParameters.hashCode();
        }
    }

}

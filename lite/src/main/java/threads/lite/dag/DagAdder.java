package threads.lite.dag;


import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.List;
import java.util.Objects;

import merkledag.pb.Merkledag;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.core.BlockStore;
import threads.lite.core.Directory;
import threads.lite.core.Link;
import threads.lite.core.Session;
import threads.lite.utils.ReaderInputStream;
import threads.lite.utils.Splitter;
import unixfs.pb.Unixfs;

public class DagAdder {

    @NonNull
    private final BlockStore blockStore;

    private DagAdder(@NonNull BlockStore blockStore) {
        this.blockStore = blockStore;
    }

    public static DagAdder createAdder(@NonNull Session session) {
        return new DagAdder(session.getBlockStore());
    }

    public Directory createDirectory(@NonNull List<Link> links) throws Exception {
        DagDirectory.DirectoryNode directory = DagDirectory.createDirectory(links);
        Block block = Block.createBlock(directory.getNode());
        blockStore.storeBlock(block);
        return new Directory(block.getCid(), directory.getSize());
    }

    public Directory createEmptyDirectory() throws Exception {
        DagDirectory.DirectoryNode directory = DagDirectory.createDirectory();
        Block block = Block.createBlock(directory.getNode());
        blockStore.storeBlock(block);
        return new Directory(block.getCid(), directory.getSize());
    }

    @NonNull
    public Directory addChild(@NonNull Merkledag.PBNode dirNode,
                              @NonNull Link link) throws Exception {

        Unixfs.Data unixData = DagReader.getData(dirNode);

        if (unixData.getType() != unixfs.pb.Unixfs.Data.DataType.Directory) {
            throw new Exception("not a directory");
        }

        long filesize = unixData.getFilesize();

        Merkledag.PBLink.Builder lnb = Merkledag.PBLink.newBuilder()
                .setName(link.getName())
                .setTsize(link.getSize());

        lnb.setHash(ByteString.copyFrom(link.getCid().bytes()));

        long newFileSize = filesize + filesize;

        Unixfs.Data.Builder builder = unixData.toBuilder();
        builder.setFilesize(newFileSize);
        builder.addBlocksizes(filesize);

        Merkledag.PBNode.Builder pbn = dirNode.toBuilder();
        pbn.addLinks(lnb.build());
        pbn.setData(ByteString.copyFrom(builder.build().toByteArray()));

        Block block = Block.createBlock(pbn.build());
        blockStore.storeBlock(block);

        return new Directory(block.getCid(), newFileSize);
    }


    public Directory removeChild(@NonNull Merkledag.PBNode dirNode, @NonNull String name) throws Exception {

        Unixfs.Data unixData = DagReader.getData(dirNode);

        if (unixData.getType() != unixfs.pb.Unixfs.Data.DataType.Directory) {
            throw new Exception("not a directory");
        }

        long filesize = 0L;

        Merkledag.PBNode.Builder pbn = dirNode.toBuilder();
        Unixfs.Data.Builder builder = unixData.toBuilder();
        builder.clearBlocksizes();

        for (int index = 0; index < pbn.getLinksCount(); index++) {
            Merkledag.PBLink link = dirNode.getLinks(index);
            if (Objects.equals(link.getName(), name)) {
                pbn.removeLinks(index);
            } else {
                filesize = filesize + link.getTsize();
                builder.addBlocksizes(link.getTsize());
            }
        }

        builder.setFilesize(filesize);
        pbn.setData(ByteString.copyFrom(builder.build().toByteArray()));
        Block block = Block.createBlock(pbn.build());
        blockStore.storeBlock(block);

        return new Directory(block.getCid(), filesize);

    }


    @NonNull
    public Cid createFromStream(@NonNull final ReaderInputStream reader) throws Exception {

        Splitter splitter = new Splitter() {

            @Override
            public int nextBytes(byte[] bytes) throws Exception {
                return reader.read(bytes);
            }

            @Override
            public boolean done() {
                return reader.done();
            }
        };

        DagWriter db = new DagWriter(blockStore, splitter);

        return db.trickle();
    }

}

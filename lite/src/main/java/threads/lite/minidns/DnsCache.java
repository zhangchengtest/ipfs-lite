/*
 * Copyright 2015-2022 the original author or authors
 *
 * This software is licensed under the Apache License, Version 2.0,
 * the GNU Lesser General Public License version 2 or later ("LGPL")
 * and the WTFPL.
 * You may choose either license to govern your use of this software only
 * upon the condition that you accept all of the terms of either
 * the Apache License 2.0, the LGPL 2.1+ or the WTFPL.
 */
package threads.lite.minidns;

import androidx.annotation.NonNull;

import java.util.LinkedHashMap;

/**
 * Cache for DNS Entries. Implementations must be thread safe.
 */
public final class DnsCache {

    /**
     * The internal capacity of the backend cache.
     */
    private final int capacity;
    /**
     * The upper bound of the ttl. All longer TTLs will be capped by this ttl.
     */
    private final long maxTTL;
    /**
     * The backend cache.
     */
    private final LinkedHashMap<DnsMessage, DnsQueryResult> backend;
    /**
     * Internal miss count.
     */
    private long missCount = 0L;
    /**
     * Internal expire count (subset of misses that was caused by expire).
     */
    private long expireCount = 0L;
    /**
     * Internal hit count.
     */
    private long hitCount = 0L;

    /**
     * Create a new LRUCache with given capacity and upper bound ttl.
     *
     * @param capacity The internal capacity.
     * @param maxTTL   The upper bound for any ttl.
     */
    public DnsCache(final int capacity, final long maxTTL) {
        this.capacity = capacity;
        this.maxTTL = maxTTL;
        backend = new LinkedHashMap<>(
                Math.min(capacity + (capacity + 3) / 4 + 2, 11), 0.75f, true) {
            @Override
            protected boolean removeEldestEntry(Entry<DnsMessage, DnsQueryResult> eldest) {
                return size() > capacity;
            }
        };
    }

    /**
     * Create a new LRUCache with given capacity.
     *
     * @param capacity The capacity of this cache.
     */
    public DnsCache(final int capacity) {
        this(capacity, Long.MAX_VALUE);
    }


    private synchronized void putNormalized(DnsMessage q, DnsQueryResult result) {
        if (result.response.receiveTimestamp <= 0L) {
            return;
        }
        backend.put(q, new DnsQueryResult(result.response));
    }


    private synchronized DnsQueryResult getNormalized(DnsMessage q) {
        DnsQueryResult result = backend.get(q);
        if (result == null) {
            missCount++;
            return null;
        }

        DnsMessage message = result.response;

        // RFC 2181 § 5.2 says that all TTLs in a RRSet should be equal, if this isn't the case, then we assume the
        // shortest TTL to be the effective one.
        final long answersMinTtl = message.getAnswersMinTtl();
        final long ttl = Math.min(answersMinTtl, maxTTL);

        final long expiryDate = message.receiveTimestamp + (ttl * 1000);
        final long now = System.currentTimeMillis();
        if (expiryDate < now) {
            missCount++;
            expireCount++;
            backend.remove(q);
            return null;
        } else {
            hitCount++;
            return result;
        }
    }


    @NonNull
    @Override
    public String toString() {
        return "DnsCache{usage=" + backend.size() + "/" + capacity + ", hits=" + hitCount +
                ", misses=" + missCount + ", expires=" + expireCount + "}";
    }

    /**
     * Add an an dns answer/response for a given dns question. Implementations
     * should honor the ttl / receive timestamp.
     *
     * @param query  The query message containing a question.
     * @param result The DNS query result.
     */
    public void put(DnsMessage query, DnsQueryResult result) {
        putNormalized(query.asNormalizedVersion(), result);
    }

    /**
     * Request a cached dns response.
     *
     * @param query The query message containing a question.
     * @return The dns message.
     */
    public DnsQueryResult get(DnsMessage query) {
        return getNormalized(query.asNormalizedVersion());
    }

}

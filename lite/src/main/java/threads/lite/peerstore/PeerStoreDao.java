package threads.lite.peerstore;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import threads.lite.cid.Peer;

@Dao
public interface PeerStoreDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPeer(Peer peer);

    @Query("SELECT * FROM Peer ORDER BY RANDOM() LIMIT :limit")
    List<Peer> getRandomPeers(int limit);
}

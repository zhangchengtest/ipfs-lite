package threads.server.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import threads.server.core.files.FILES;
import threads.server.core.files.FilesDatabase;
import threads.server.core.files.Proxy;
import threads.server.core.files.SortOrder;

public class LiteViewModel extends AndroidViewModel {
    @NonNull
    private final MutableLiveData<SortOrder> sortOrder = new MutableLiveData<>(SortOrder.DATE);
    @NonNull
    private final MutableLiveData<Long> parentProxy = new MutableLiveData<>(0L);
    @NonNull
    private final MutableLiveData<String> query = new MutableLiveData<>("");
    @NonNull
    private final MediatorLiveData<Void> liveDataMerger = new MediatorLiveData<>();

    private final FilesDatabase filesDatabase;

    public LiteViewModel(@NonNull Application application) {
        super(application);

        filesDatabase = FILES.getInstance(
                application.getApplicationContext()).getFilesDatabase();

        liveDataMerger.addSource(parentProxy, value -> liveDataMerger.setValue(null));
        liveDataMerger.addSource(query, value -> liveDataMerger.setValue(null));
        liveDataMerger.addSource(sortOrder, value -> liveDataMerger.setValue(null));
    }

    @NonNull
    private SortOrder getSortOrderValue() {
        return Objects.requireNonNull(sortOrder.getValue());
    }

    @NonNull
    public MutableLiveData<SortOrder> getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(@NonNull SortOrder sortOrder) {
        getSortOrder().postValue(sortOrder);
    }

    @NonNull
    public MutableLiveData<String> getQuery() {
        return query;
    }

    public void setQuery(@NonNull String query) {
        getQuery().postValue(query);
    }

    @NonNull
    private String getQueryValue() {
        MutableLiveData<String> queryData = getQuery();
        String query = queryData.getValue();
        String searchQuery = "";
        if (query != null) {
            searchQuery = query.trim();
        }
        if (!searchQuery.startsWith("%")) {
            searchQuery = "%" + searchQuery;
        }
        if (!searchQuery.endsWith("%")) {
            searchQuery = searchQuery + "%";
        }
        return searchQuery;
    }


    @NonNull
    public MutableLiveData<Long> getParentProxy() {
        return parentProxy;
    }

    public void setParentProxy(long idx) {
        getParentProxy().postValue(idx);
    }

    @NonNull
    public LiveData<List<Proxy>> getLiveDataFiles() {
        return Transformations.switchMap(
                liveDataMerger, input -> {
                    SortOrder sortOrder = getSortOrderValue();
                    switch (sortOrder) {
                        case DATE: {
                            return Transformations.map(filesDatabase.proxyDao().getLiveDataFiles(
                                    getParentProxyValue(), getQueryValue()), input1 -> {
                                input1.sort(Comparator.comparing(Proxy::getLastModified).reversed());
                                return input1;
                            });
                        }
                        case DATE_INVERSE: {
                            return Transformations.map(filesDatabase.proxyDao().getLiveDataFiles(
                                    getParentProxyValue(), getQueryValue()), input1 -> {
                                input1.sort(Comparator.comparing(Proxy::getLastModified));
                                return input1;
                            });
                        }
                        case SIZE: {
                            return Transformations.map(filesDatabase.proxyDao().getLiveDataFiles(
                                    getParentProxyValue(), getQueryValue()), input1 -> {
                                input1.sort(Comparator.comparing(Proxy::getSize));
                                return input1;
                            });
                        }
                        case SIZE_INVERSE: {
                            return Transformations.map(filesDatabase.proxyDao().getLiveDataFiles(
                                    getParentProxyValue(), getQueryValue()), input1 -> {
                                input1.sort(Comparator.comparing(Proxy::getSize).reversed());
                                return input1;
                            });
                        }
                        case NAME: {
                            return Transformations.map(filesDatabase.proxyDao().getLiveDataFiles(
                                    getParentProxyValue(), getQueryValue()), input1 -> {
                                input1.sort(Comparator.comparing(Proxy::getName));
                                return input1;
                            });
                        }
                        case NAME_INVERSE: {
                            return Transformations.map(filesDatabase.proxyDao().getLiveDataFiles(
                                    getParentProxyValue(), getQueryValue()), input1 -> {
                                input1.sort(Comparator.comparing(Proxy::getName).reversed());
                                return input1;
                            });
                        }
                        default:
                            return filesDatabase.proxyDao().getLiveDataFiles(
                                    getParentProxyValue(), getQueryValue());
                    }

                });
    }

    public long getParentProxyValue() {
        Long value = getParentProxy().getValue();
        return Objects.requireNonNull(value); // this should never be null
    }


}